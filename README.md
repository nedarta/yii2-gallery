Yii2 multilingual gallery with categories
=========================================
Yii2 multilingual gallery with categories

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist nedarta/yii2-gallery "*"
```

or add

```
"nedarta/yii2-gallery": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \nedarta\gallery\AutoloadExample::widget(); ?>```